# enconding: utf8
from django.forms import ModelForm
from django import forms
from principal.models import Receta, Comentario

class ContactoForm(forms.Form):
	correo = forms.EmailField(label="E-mail")
	mensaje = forms.CharField(widget=forms.Textarea)

class RecetaForm(ModelForm):
	class Meta:
		model = Receta
		widgets = {'imagen': forms.FileInput(attrs={'id': 'inputFile', 'accept': 'image/jpg, image/png'}) }
		fields = ('titulo', 'ingredientes', 'prepacion', 'usuario','imagen',)
		exclude = ('tiempo_registro', )
			
class ComentarioForm(ModelForm):
	class Meta:
		model = Comentario
		fields = ('texto', 'receta')
		exclude = ('usuario',)
