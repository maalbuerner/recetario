# encoding=utf8
from principal.models import Receta, Comentario
from principal.forms import RecetaForm, ComentarioForm, ContactoForm
from django.contrib.auth.models import User
from django.shortcuts import render_to_response, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.template import RequestContext
from django.core.mail import EmailMessage
from django.core.urlresolvers import reverse
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import login, authenticate, logout
# from django.contrib.auth.forms.decorators import login_required


def sobre(request):
	return render_to_response('sobre.html');

def inicio(request):
	recetas = Receta.objects.all()
	return render_to_response('inicio.html',{'recetas':recetas})

def usuarios(request):
	usuarios = User.objects.all()
	recetas = Receta.objects.all()
	return render_to_response('usuarios.html', {'usuarios': usuarios, 'recetas': recetas})

def recetas(request):
	recetas = Receta.objects.all()
	return render_to_response('recetas.html', {'recetas': recetas}, context_instance=RequestContext(request))

def detalle_receta(request, receta_id):
	receta = get_object_or_404(Receta, pk=receta_id)
	comentarios = Comentario.objects.filter(receta=receta)
	return render_to_response('receta.html', {'receta': receta, 'comentarios': comentarios},
		context_instance=RequestContext(request))

def contacto(request):
	if request.method=='POST':
		formulario = ContactoForm(request.POST)
		if formulario.is_valid():
			titulo = 'Mensaje desde el Recetario Django 2020'
			contenido = formulario.cleaned_data['mensaje'] + '\n'
			contenido += 'Comunicarse a: ' + formulario.cleaned_data['correo']
			correo = EmailMessage(titulo, contenido, to=['malbuerner89@nauta.cu'])
			correo.send()
			return HttpResponseRedirect('/');
	else:
		formulario = ContactoForm()
	return render_to_response('contacto.html', {'formulario': formulario}, context_instance=RequestContext(request))

def new_receta(request):
	print('request.method: '+request.method)
	if request.method == 'POST':
		formulario = RecetaForm(request.POST, request.FILES)
		if formulario.is_valid():
			formulario.save()
			return HttpResponseRedirect('/recetas')
	else:
		formulario = RecetaForm()
	return render_to_response('recetaform.html', {'formulario': formulario}, context_instance=RequestContext(request))

def edit_receta(request, receta_id):
	receta = Receta.objects.get(id=receta_id)
	if request.method == 'POST':
	    formulario = RecetaForm(request.POST, request.FILES, instance=receta)
	    if formulario.is_valid():
	        formulario.save()
	    else:
	        print('invalid')
	    return HttpResponseRedirect(reverse('detalle_receta', args=(receta_id,)))
	formulario = RecetaForm(instance=receta)
	return render_to_response('edit_receta.html', {'receta': receta, 'formulario': formulario},  context_instance=RequestContext(request))

def delete_receta(request, receta_id):
	receta = Receta.objects.get(id=receta_id)
	if receta is not None:
		comentarios = Comentario.objects.filter(receta=receta)
		for c in comentarios:
			c.delete()
		receta.delete()
	return HttpResponseRedirect('/recetas')

def new_comentario(request):
	if request.method == 'POST':
		comentario = Comentario()
		comentario.usuario = request.user
		formulario = ComentarioForm(request.POST, instance=comentario)
		if formulario.is_valid():
			formulario.save()
			return HttpResponseRedirect('/recetas')	
	else:
		formulario = ComentarioForm()
	return render_to_response('comentarioform.html', {'formulario': formulario}, context_instance=RequestContext(request))

def new_comentario_receta(request):
	if request.method == 'POST':
		receta_id = request.POST['receta_id']
		comentario = Comentario()
		comentario.receta = Receta.objects.get(pk=receta_id)
		comentario.texto = request.POST['texto']
		comentario.usuario = request.user
		comentario.save()
	return HttpResponseRedirect(reverse('detalle_receta', args=(receta_id,)));	

def new_user(request):
	if request.method == 'POST':
		form = UserCreationForm(request.POST)
		if form.is_valid():
			form.save()
			return HttpResponseRedirect('/')
	else:
		form = UserCreationForm()
	return render_to_response('new_user.html', {'form': form}, context_instance=(RequestContext(request)))	