from django.conf.urls import include, url
from django.contrib import admin
from django.conf import settings

urlpatterns = [
	url(r'^$', 'principal.views.inicio', name='inicio'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^sobre/$', 'principal.views.sobre', name='sobre'),
    url(r'^usuarios/$', 'principal.views.usuarios', name='usuarios'),
    url(r'^user/new/$', 'principal.views.new_user', name='new_user'),
    url(r'^recetas/$', 'principal.views.recetas', name='recetas'),
    url(r'^receta/new/$', 'principal.views.new_receta', name='new_receta'),
    url(r'^comentario/new/$', 'principal.views.new_comentario', name='new_comentario'),
    url(r'^comentario/receta/new/$', 'principal.views.new_comentario_receta', name='new_comentario_receta'),
    url(r'^contacto/$', 'principal.views.contacto', name='contacto'),
    url(r'^receta/(?P<receta_id>\d+)$', 'principal.views.detalle_receta', name='detalle_receta'),
    url(r'^receta/edit/(?P<receta_id>\d+)$', 'principal.views.edit_receta', name='edit_receta'),
    url(r'^receta/delete/(?P<receta_id>\d+)$', 'principal.views.delete_receta', name='delete_receta'),
    url(r'^media/(?P<path>.*)$','django.views.static.serve', {'document_root':settings.MEDIA_ROOT,}),
]